import json
import matplotlib as plt
import numpy as np


def evaluate_model(build_model, x_train, y_train, x_test, y_test):
    epoch_convergence = []
    train_loss = []
    train_accuracy = []
    test_loss = []
    test_accuracy = []

    model_history_of_last_run = []

    # build and train model
    for i in range(5):
        model = build_model()

        model_history = model.fit(x_train, y_train,
                                  batch_size=128,
                                  epochs=20,
                                  verbose=0,
                                  validation_data=(x_test, y_test))

        # Report model summary once
        if i == 0:
            model.summary()

        epoch_number_after_train_accuracy_convergence = find_stable_epoch(model_history, 'accuracy')
        epoch_number_after_test_accuracy_convergence = find_stable_epoch(model_history, 'val_accuracy')

        epoch_number_after_model_convergence = max(epoch_number_after_train_accuracy_convergence,
                                                   epoch_number_after_test_accuracy_convergence)

        # evaluate model
        score = model.evaluate(x_test, y_test, verbose=0)

        # Append data
        epoch_convergence.append(epoch_number_after_model_convergence)
        train_loss.append(model_history.history['loss'][-1])
        train_accuracy.append(model_history.history['accuracy'][-1])
        test_loss.append(score[0])
        test_accuracy.append(score[1])

        # Save a sample of the training history
        if i == 4:
            model_history_of_last_run = model_history

    average_epoch_after_convergence = np.average(epoch_convergence)
    average_loss_train = np.average(train_loss)
    average_accuracy_train = np.average(train_accuracy)
    average_loss_test = np.average(test_loss)
    average_accuracy_test = np.average(test_accuracy)

    return ({'Average epoch after convergence': average_epoch_after_convergence,
             'Average training loss': average_loss_train,
             'Average training accuracy': average_accuracy_train,
             'Average test loss': average_loss_test,
             'Average test accuracy': average_accuracy_test}, model_history_of_last_run)


def find_stable_epoch(model_history, identifier):
    identical_accuracy_count = 0
    last_accuracy_value = 0
    stable_epoch_number = 0
    for index, accuracy in enumerate(model_history.history[identifier]):
        if last_accuracy_value == accuracy:
            identical_accuracy_count += 1
        if identical_accuracy_count == 4:
            stable_epoch_number = index + 1
            break
        last_accuracy_value = accuracy

    if stable_epoch_number == 0:
        stable_epoch_number = -1

    return stable_epoch_number


def visualize_training(model_history, model_name):
    # visualize history for accuracy
    plt.plot(model_history.history['accuracy'])
    plt.plot(model_history.history['val_accuracy'])
    plt.ylabel('model accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='best')
    plt.title('Accuracy of {model_name} w.r.t. training epochs'.format(model_name=model_name))
    plt.show()
    # visualize history for loss
    plt.plot(model_history.history['loss'])
    plt.plot(model_history.history['val_loss'])
    plt.ylabel('model loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='best')
    plt.title('Loss of {model_name} w.r.t. training epochs'.format(model_name=model_name))
    plt.show()


def report_and_visualize_model_performance(build_model, x_train, y_train, x_test, y_test, model_name):
    performance_report, model_history_sample = evaluate_model(build_model, x_train, y_train, x_test, y_test)
    visualize_training(model_history_sample, model_name)
    print(json.dumps(performance_report, indent=2))
